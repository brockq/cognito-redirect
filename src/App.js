import React from 'react';
import logo from './logo.svg';
import './App.css';
import Amplify, { Auth } from 'aws-amplify';
// import awsconfig from './aws-exports';
import { withAuthenticator, Authenticator } from 'aws-amplify-react';
// Amplify.configure(awsconfig);
Amplify.configure({
  Auth: {

      // REQUIRED only for Federated Authentication - Amazon Cognito Identity Pool ID
      // identityPoolId: 'ap-southeast-1:3f74df18-1fb1-49ed-9117-c133a50876f2',
      
      // REQUIRED - Amazon Cognito Region
      region: 'ap-southeast-1',

      // OPTIONAL - Amazon Cognito Federated Identity Pool Region 
      // Required only if it's different from Amazon Cognito Region
      // identityPoolRegion: 'XX-XXXX-X',

      // OPTIONAL - Amazon Cognito User Pool ID
      userPoolId: 'ap-southeast-1_fAppaGHAb',

      // OPTIONAL - Amazon Cognito Web Client ID (26-char alphanumeric string)
      userPoolWebClientId: 'gprgqu2btpdbqt9o0kg3ee7mf',

      // OPTIONAL - Enforce user authentication prior to accessing AWS resources or not
      mandatorySignIn: false,

      // OPTIONAL - Configuration for cookie storage
      // Note: if the secure flag is set to true, then the cookie transmission requires a secure protocol
      // cookieStorage: {
      // // REQUIRED - Cookie domain (only required if cookieStorage is provided)
      //     domain: 'http://localhost:3001',
      // // OPTIONAL - Cookie path
      //     path: '/',
      // // OPTIONAL - Cookie expiration in days
      //     expires: 365,
      // // OPTIONAL - Cookie secure flag
      // // Either true or false, indicating if the cookie transmission requires a secure protocol (https).
      //     // secure: true
      // },
      // // OPTIONAL - customized storage object
      // storage: new MyStorage(),
      // // OPTIONAL - Manually set the authentication flow type. Default is 'USER_SRP_AUTH'
      // authenticationFlowType: 'USER_PASSWORD_AUTH',
      // // OPTIONAL - Manually set key value pairs that can be passed to Cognito Lambda Triggers
      // clientMetadata: { myCustomKey: 'myCustomValue' }
  }
});

// const signUpConfig = {
//   header: 'My Customized Sign Up',
//   hideAllDefaults: true,
//   defaultCountryCode: '852',
//   signUpFields: [
//     {
//       label: 'My custom email label',
//       key: 'email',
//       required: true,
//       displayOrder: 1,
//       type: 'string'
//     },
//     // ... // and other custom attributes
//   ]
// };
// console.log(Auth.configure());

function App() {
  return (
    <div className="App">
      <header className="App-header">
        {/* <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a> */}
        <Authenticator 
          usernameAttributes='email'
          // signUpConfig={signUpConfig}
          onStateChange={(authState, data) => {
            console.log(authState, data);
          }}
        />
      </header>
    </div>
  );
}

// export default withAuthenticator(App, true);
export default App;